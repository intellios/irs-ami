// import Vue from 'vue'
// import Vuetify from 'vuetify'
// import 'vuetify/dist/vuetify.min.css'

// Vue.use(Vuetify, {
//   iconfont: 'mdi',
// })

import Vue from 'vue'
import Vuetify from 'vuetify'


Vue.use(Vuetify)
export default new Vuetify({ theme:{
  dark: true,
    themes: {
      light: {
        primary: '#42a5f5',
      },
      dark: {
        primary: '#2196F3',
      },
    },
  },
  options: {
    customProperties: true,
  },
  icons: {
    iconfont: 'md', // default is 'mdi'
  }
})