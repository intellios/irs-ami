// import '@babel/polyfill'
import Vue from "vue";
import './plugins/vuetify'
import vuetify from '@/plugins/vuetify';
// import vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import App from "./App.vue";
import router from "./router";
import store from "./store";
// import VueTable from 'spreadsheet-vuejs';

  import splitPane from 'vue-splitpane'
  Vue.component('split-pane', splitPane);

Vue.config.productionTip = false;

// Vue.use(Vuetify)

new Vue({
  router,
  store,
  vuetify,
  // Vuetify,
  render: h => h(App)
}).$mount("#app");

// new Vue({
// 	el: '#app',
// 	render: h => h(App)
// })
